import { Component, OnInit } from '@angular/core';
import { MemberService } from '../services/member.service';
import { Member } from '../model/member.model';
import {MatButtonModule} from '@angular/material/button'; 

@Component({
  selector: 'app-members-list',
  templateUrl: './members-list.component.html',
  styleUrls: ['./members-list.component.scss']
})


export class MembersListComponent implements OnInit {
  members: Member[];
  tableHeaders: string[] = ['id', 'firstname', 'lastname', 'avatar', 'function','description'];

  constructor(private memberService : MemberService ) { }

 ngOnInit(){
  this.getAllMembers();
     this.memberService.getAllMembers().subscribe(data => {
          console.log(data);
      });
 }

 getAllMembers() {
  this.memberService.getAllMembers().subscribe((members: Member[]) => {
    this.members = members;
  });
}

}
