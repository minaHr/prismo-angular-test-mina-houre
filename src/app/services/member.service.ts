import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Member } from '../model/member.model'; 
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
   constructor(private http: HttpClient){}    

   // récupérer tous les membres 
    public getAllMembers() {
        return this.http.get("./assets/members.json");
    }
    // récupérer un member par son Id
    public getMemberById(id: number){
        return this.getAllMembers()
          .pipe(map((member: any) => {
            return member.find(data => data.id === id)
          }));
      }

      createMember(member: Member) { 

      }
     
// stocker un member dans fichier json
  public saveText(text, filename){
  var a = document.createElement('a');
  a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(text));
  a.setAttribute('download', filename);
  a.click()
}
    
}