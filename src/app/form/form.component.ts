import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Member } from '../model/member.model'
import { MemberService } from '../services/member.service';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatInputModule} from '@angular/material/input'; 
import { Router } from '@angular/router';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  MemberForm: FormGroup;
  member: Member;

  constructor(private fb: FormBuilder, private memberService: MemberService, private route: Router) {
    this.MemberForm = this.fb.group({
      firstname: [''],
      lastname: [''],
      avatar: [''],
      function: [''],
      description: ['']
    });
  }

  ngOnInit(): void {
  }
submitMember() {
  console.log(this.MemberForm.value);
  this.route.navigate(['welcome']);   }
}
