import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { MembersListComponent } from './members-list/members-list.component';
import { MemberComponent } from './member/member.component';
import { FormComponent } from './form/form.component';


const routes: Routes = [
  { path:'welcome',component: WelcomeComponent},
  { path:'',component: WelcomeComponent},
  { path:'members-list',component: MembersListComponent},
  { path:'member/:id',component: MemberComponent},
  { path:'create-member',component: FormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
