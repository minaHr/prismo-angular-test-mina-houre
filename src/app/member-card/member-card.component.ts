import { Component, OnInit ,Input} from '@angular/core';
import {MatCardModule} from '@angular/material/card'; 
import { Member } from '../model/member.model';
import {MatButtonModule} from '@angular/material/button'; 

@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss']
})
export class MemberCardComponent {
 
  @Input() member: Member;

  constructor() { }

  

}
