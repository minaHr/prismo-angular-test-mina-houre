import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MemberService } from '../services/member.service';
import { Member} from '../model/member.model';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {
id: number;
member :Member;

constructor(
  private route: ActivatedRoute, 
  private router: Router ,
  private memberService : MemberService,

) {} 
ngOnInit() { 
  const Id = this.route.snapshot.paramMap.get('id');
  this.id=parseInt(Id);
  this.memberService.getMemberById(this.id).subscribe((member:Member) => {
    this.member = member
});
}
}
