export class Member {
    id: string;
    firstname: string;
    lastname: string;
    avatar: string;
    function: string;
    description: string;
}
